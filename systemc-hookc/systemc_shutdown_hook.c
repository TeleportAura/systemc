#define _GNU_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <dlfcn.h>

int * __error(void);


static int ipc_socket;

void catch_term(int sig);

void send_fd(int usock, int fildes)
{
    union {
        char buf[CMSG_SPACE(sizeof(int))];
        struct cmsghdr useless;
    } controlMsg;
    
    struct msghdr msgh;
    msgh.msg_name = NULL;
    msgh.msg_namelen = 0;

    struct iovec iovec;
    char fake_data = 42;
    iovec.iov_base = &fake_data;
    iovec.iov_len = sizeof(int);
    msgh.msg_iov = &iovec;
    msgh.msg_iovlen = 1;

    msgh.msg_control = controlMsg.buf;
    msgh.msg_controllen = sizeof(controlMsg.buf);

    struct cmsghdr *cmsgp;
    cmsgp = CMSG_FIRSTHDR(&msgh);
    cmsgp->cmsg_level = SOL_SOCKET;
    cmsgp->cmsg_type = SCM_RIGHTS;
    cmsgp->cmsg_len = CMSG_LEN(sizeof(int));
    
    memcpy(CMSG_DATA(cmsgp), &fildes, sizeof(int));

    if (sendmsg(usock, &msgh, 0) == -1) {
        perror("Failed to send FD to supervisor");
    }
}
