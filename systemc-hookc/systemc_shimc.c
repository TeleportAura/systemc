/* TODO experiment with how systemc_token should be saved in regards to possible
   untrusted child processes */
// idea: save token in a file and xor with env variable or something

#define _GNU_SOURCE

#include <dlfcn.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

void so_entry_point() __attribute__((constructor));
void so_exit_point() __attribute__((destructor));

void handle_sigterm();

static void *startup_hooks;
static void *shutdown_hooks;

// TODO propagate these to the hooking libraries so they can use them as well
#include<sys/socket.h>
static int(*bind_fn_ptr)(int sockfd, const struct sockaddr *addr, socklen_t addrlen) = &bind;
static int(*accept4_fn_ptr)(int sockfd, struct sockaddr *restrict addr, socklen_t *restrict addrlen, int flags) = &accept4;

void so_entry_point()
{
    struct sigaction act;
    sigaction(SIGTERM, NULL, &act);
    if ((act.sa_handler == NULL || (void *) act.sa_handler == SIG_DFL) && (act.sa_sigaction == NULL || (void *) act.sa_sigaction == SIG_DFL)) {
        act.sa_handler = handle_sigterm;
        sigaction(SIGTERM, &act, NULL);
    }

    if (!startup_hooks) {
        startup_hooks = dlopen("libsystemcstartuphook.so", RTLD_NOW);
        if (!startup_hooks) {
            printf("[systemc] Failed to load startup hooks!\n");
            printf(dlerror());
            printf("\n");
        }
        bind_fn_ptr = dlsym(startup_hooks, "bind");
        accept4_fn_ptr = dlsym(startup_hooks, "accept4");
    }
    if (!shutdown_hooks) {
        shutdown_hooks = dlopen("libsystemcshutdownhook.so", RTLD_NOW);
        if (!shutdown_hooks) {
            printf("[systemc] Failed to load shutdown hooks!\n");
            printf(dlerror());
            printf("\n");
        }
    }

    /*systemc_token = getenv("SYSTEMC_SESSION_TOKEN") ;
    if (systemc_token) {
        systemc_token = strdup(systemc_token);
        unsetenv("SYSTEMC_SESSION_TOKEN");
    }*/
}

void so_exit_point()
{
    //if (systemc_token) {
        //setenv("SYSTEMC_SESSION_TOKEN", systemc_token, 1);
        char *args[] = {"systemc-stopc", NULL};
        execvp("systemc-stopc", args);
        perror("Failed to execute systemc-stopc");
    //}
}

void handle_sigterm(int sig) {
    so_exit_point();
}

void partial_unload() {
    dlclose(startup_hooks);
    bind_fn_ptr = &bind;
}

int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
    bind_fn_ptr(sockfd, addr, addrlen);
}

int accept4(int sockfd, struct sockaddr *restrict addr, socklen_t *restrict addrlen, int flags)
{
    accept4_fn_ptr(sockfd, addr, addrlen, flags);
}
