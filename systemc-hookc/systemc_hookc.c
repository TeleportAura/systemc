#define _GNU_SOURCE

#include <dlfcn.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <errno.h>
#include <stdint.h>
#include <stdbool.h>

int receive_fildes(int socket, int *fildes, size_t fildes_len, char *data, size_t data_len);

int * __error(void);

void cpsockopts(int oldsock, int newsock);

static int ipc_connection = 4;

int bind(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
    static int(*libc_bind_fn)(int, const struct sockaddr*, socklen_t) = NULL;
    if (!libc_bind_fn) {
        libc_bind_fn = dlsym(RTLD_NEXT, "bind");
    }

    if ((*addr).sa_family == AF_INET) {
        if (ipc_connection) {
            uint8_t opcode = 1;
            if (write(ipc_connection, &opcode, 1) == 1) {
                write(ipc_connection, &(((struct sockaddr_in*)addr)->sin_port), sizeof(((struct sockaddr_in*)addr)->sin_port));
                int fildes = 1337;
                if (receive_fildes(ipc_connection, &fildes, 1, &opcode, 1) == -1) {
                    if (opcode == 2) {
                        ipc_connection = 0;
                    }
                    if (opcode != 1) {
                        fprintf(stderr, "opcode: %d");
                        perror("Caught error while trying to receive Fd (1)");
                    }
                    return libc_bind_fn(sockfd, addr, addrlen);
                }
                if (dup2(fildes, sockfd) == -1) {
                    perror("Caught error while receiving Fd (1)");
                    return -1;
                }
                write(ipc_connection, &sockfd, sizeof(sockfd));
                return 0;
            } else {
                ipc_connection = 0;
            }
        }
    }

    return libc_bind_fn(sockfd, addr, addrlen);

}

//TODO intercept accept and accept4
int accept4(int sockfd, struct sockaddr *restrict addr, socklen_t *restrict addrlen, int flags) {
    static int (*accept4_libc_fn)(int sockfd, struct sockaddr *restrict addr, socklen_t *restrict addrlen, int flags) = NULL;
    if (!accept4_libc_fn) {
        accept4_libc_fn = dlsym(RTLD_NEXT, "accept4");
    }

    if (ipc_connection) {
        uint8_t opcode = 2;
        if (write(ipc_connection, &opcode, 1) == 1) {
            write(ipc_connection, &sockfd, sizeof(sockfd));
            int fildes = 1337;
            if (receive_fildes(ipc_connection, &fildes, 1, &opcode, 1) == -1) {
                if (opcode == 2) {
                    ipc_connection = 0;
                }
                if (opcode != 1) {
                    fprintf(stderr, "opcode: %d");
                    perror("Caught error while trying to receive Fd (2)");
                }
                return accept4_libc_fn(sockfd, addr, addrlen, flags);
            }
            if (getsockname(fildes, addr, addrlen) == -1) {
                perror("Received an invalid socket!");
            }
            return fildes;
        } else {
            ipc_connection = 0;
        }
    }

    return accept4_libc_fn(sockfd, addr, addrlen, flags);
}

//TODO intercept select, poll and epoll

void cpsockopts(int oldsock, int newsock) {
    fcntl(newsock, F_SETFL, fcntl(oldsock, F_GETFL));
    ioctl(newsock, FIONBIO, 0);
}
