#!/bin/sh

gcc src/main.c -o systemc-stopc -Wall -Wpedantic
mv systemc-stopc ~/.local/bin/systemc/systemc-stopc
