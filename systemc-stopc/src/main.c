#include <stdlib.h>
#include <signal.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <dirent.h>

#define SOCKET_PATH "/tmp/systemc/scheduler.sock"

void close_leftover_fds();

int main() {

    int sock = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock == -1) {
        perror("Failed to create socket");
        return -1;
    }

    struct sockaddr_un addr;
    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wstringop-truncation"
    strncpy(addr.sun_path, SOCKET_PATH, sizeof(SOCKET_PATH) - 1);
    #pragma GCC diagnostic pop

    if (connect(sock, (const struct sockaddr *) &addr, sizeof(addr)) == -1) {
        perror("Failed to connect to address");
        return -1;
    }

    
    int8_t buf = 2;
    write(sock, &buf, 1);
    unsigned long token = strtoul(getenv("SYSTEMC_SESSION_TOKEN"), NULL, 10);
    write(sock, &token, sizeof(token));
    read(sock, &buf, 1);
    switch (buf) {
        case 1: ;
            pid_t pgid = getpgrp();
            write(sock, &pgid, sizeof(pgid));
            close_leftover_fds();
            char* newargv[] = {"systemc-sleepc", NULL};
            execvp("systemc-sleepc", newargv);
            perror("Failed to execute systemc-sleepc");
            return -1;
        case -1:
            exit(SIGTERM);
        default:
            fprintf(stderr, "An unknown error occured: %d", buf);
            return -1;
    }

}

void close_leftover_fds() {
    DIR *fd_dir = opendir("/proc/self/fd");
    for (int i = 0; i < 5; i++) {
        readdir(fd_dir);
    }
    struct dirent *entry = readdir(fd_dir);
    while (entry) {
        int fd = atoi(entry->d_name);
        if (fd) {
            close(fd);
        }
        entry = readdir(fd_dir);
    }
}
