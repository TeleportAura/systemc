use std::{fs, ptr, collections::HashMap, time::Instant, ffi::{CStr, c_int}};

use libc::pid_t;

use crate::task::Cache;

#[macro_export]
macro_rules! default_handle_read_err {
    ($err:expr, $socket_name:expr, $recovery:expr) => {
        match $err.kind() {
            ErrorKind::WouldBlock => {}
            ErrorKind::UnexpectedEof => {
                ($recovery);
                continue;
            }
            _ => {
                eprintln!("Caught error while reading packet from {}: {:?}", $socket_name, $err);
                ($recovery);
                continue;
            }
        }
    }
}

//this only needs to be done a few times an hour
pub fn discover_process_passive_sockets(cache: &mut Cache, process_groups: &[pid_t]) {
    //todo use vec instead
    let mut processes = HashMap::<pid_t, Vec<(pid_t, Vec<u64>)>>::new();
    for group in process_groups {
        processes.insert(*group, Vec::new());
    }
    let proc = fs::read_dir("/proc/").unwrap();
    for dir_entry in proc {
        if let Ok(dir_entry) = dir_entry {
            let pid = dir_entry.file_name();
            if let Some(pid) = pid.to_str() {
                if let Ok(pid) = pid.parse::<pid_t>() {
                    let stat = fs::read_to_string(dir_entry.path().to_str().unwrap().to_owned() + "/stat");
                    if let Ok(stat) = stat {
                        let pgroup = stat.split_whitespace().nth(4).unwrap().trim().parse();
                        if let Ok(pgroup) = pgroup {
                            match processes.get_mut(&pgroup) {
                                Some(pids) => {
                                    let mut inodes = Vec::new();
                                    match fs::read_dir(format!("/proc/{}/fd", pid)) {
                                        Ok(read_dir) => {
                                            for dir_entry in read_dir {
                                                if let Ok(dir_entry) = dir_entry {
                                                    let resource = fs::read_link(dir_entry.path()).unwrap();
                                                    let resource_str = resource.to_str().unwrap();
                                                    if resource_str.starts_with("socket:") {
                                                        let close_bracket_index = resource_str.find("]").unwrap();
                                                        inodes.push(resource_str[8..close_bracket_index].parse().unwrap())
                                                    }
                                                }
                                            }
                                        }
                                        Err(_) => {}
                                    }
                                    pids.push((pid, inodes));
                                }
                                _ => {
                                    continue;
                                }
                            }
                        } else {
                            continue;
                        }
                    }
                }
            }
        }
    }

    let mut passive_socket_inodes = Vec::new();
    let tcp_socks = fs::read_to_string("/proc/net/tcp").unwrap();
    let mut lines = tcp_socks.lines();
    lines.next();
    for line in lines {
        if line[20..33] == *"00000000:0000" {
            let inode = &line[91..line.len()];
            let inode = inode.split_once(" ").unwrap().0.parse().unwrap();
            passive_socket_inodes.push(inode);
            continue;
        }
        break;
    }

    let instant = Instant::now();
    cache.process_groups.clear();
    for (pgid, pids) in processes {
        let mut cached_pids = Vec::new();
        for (pid, inodes) in pids {
            let mut cached_inodes = Vec::new();
            for inode in inodes {
                let l = passive_socket_inodes.len();
                for i in (0..l).rev() {
                    if inode == *passive_socket_inodes.get(i).unwrap() {
                        passive_socket_inodes.swap_remove(i);
                        cached_inodes.push(inode);
                    }
                }
            }
            cached_pids.push((pid, cached_inodes))
        }
        cache.process_groups.push((pgid, cached_pids, instant));
    }

}


pub fn get_ports_for_inodes(process_group: (&pid_t, &Vec<(pid_t, Vec<u64>)>)) -> Vec<u32> {
    let mut ports = 0;
    for (_, inodes) in process_group.1 {
        ports += inodes.len();
    }
    let mut ports = Vec::with_capacity(ports);
    let tcp_socks = fs::read_to_string("/proc/net/tcp").unwrap();
    let mut lines = tcp_socks.lines();
    lines.next();
    for line in lines {
        if line[20..33] == *"00000000:0000" {
            let len = line.len();
            let port = &line[15..19];
            let port = u32::from_str_radix(port, 16).unwrap();
            let inode = &line[91..len];
            let inode = inode.split_once(" ").unwrap().0.parse().unwrap();
        
            for (_, inodes) in process_group.1 {
                for inode2 in inodes {
                    if *inode2 == inode {
                        ports.push(port);
                    }
                }
            }
        } else {
            break;
        }
    }
    ports
}

pub fn read_dir_at<F: FnMut(&CStr)>(fd: c_int, mut f: F) -> Result<(), std::io::Error> {
    let dir_stream = unsafe {
        libc::fdopendir(fd)
    };
    if dir_stream == ptr::null_mut() {
        return Err(std::io::Error::last_os_error());
    }
    let mut dirent = unsafe {
        // directly skips over the first two directories, ie. ".." and "."
        // I don't even want to know how non-portable this fix is
        for _ in 0..2 {
            libc::readdir(dir_stream);
        }
        libc::readdir(dir_stream)
    };
    while dirent != ptr::null_mut() {
        let dir_name = unsafe { CStr::from_ptr(&(*dirent).d_name as *const _) };
        f(&dir_name);
        dirent = unsafe {
            libc::readdir(dir_stream)
        };
    }
    
    Ok(())
}
