use std::{time::{Duration, Instant}, thread::sleep};

use task::Scheduler;

pub mod task;
pub mod util;
pub mod os_helper;

#[cfg(test)]
mod test;


//FIXME fd leaks and systemc-startc not being reaped 
//FIXME document somewhere that scripts should always use exec

// TODO include checks with all allocations that a client could influence (Vec::with_capacity, etc)
// TODO have some concept of daemon id so that multiple daemons could theoretically be ran - i have no clue why
// one would do that but whatever
// TODO have some kind of concept of "multi root setups" where we have (maybe a tree of ?)  master nodes with their children
// and whenever a master needs something to start on a children (either because it was configured to do so or because there
// were no resources left, also TODO make the latter configurable on a per-application basis in terms of how much it needs)
// it just waits for that node to connect and that's kinda it
// TODO test around whether this could be used as a MC cloud or at least as part of a modular high-performance cloud system
// adhering to the UNIX philosophy (probably yes, new project inc)
// TODO make it possible to use systemc even without the hooking itself working, instead "redirect" the execution address
// (like jmp) after a ptrace call so that the code that handles everything else is still called
// TODO make complex distributed systems possible by basically always handling the passive socket and handing the active socket over
// (this would then depend on hooking, however) - though it is questionable whether this really needs to be part of systemc
// but then again, a lot of the infrastructure would be already here and one could mix-match it with hibernation enabled or disabled
// and multiple instances enabled or disabled (this would in practice allow us to better scale services and together with switches and
// multi-root could be a game changer)
// TODO powerful stdin/stdout interface for the daemon to change basically anything at any time (allows for more complex systems using systemc)

fn main() {
    let mut time;
    let mut scheduler = Scheduler::new().unwrap();
    loop {
        time = Instant::now();
        scheduler.update();
        sleep(Duration::from_millis(50).saturating_sub(time.elapsed()));
    }
}
