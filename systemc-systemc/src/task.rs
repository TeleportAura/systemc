use std::{ffi::CString, os::unix::{net::{UnixListener, UnixStream}, prelude::{IntoRawFd, AsRawFd}}, net::{TcpListener, TcpStream}, time::{Instant, Duration}, thread::sleep,
          io::{ErrorKind, Read, Write}, mem, fs, collections::HashMap};

use libc::{pid_t, SIGUSR1, SIGTERM, SIGCONT};

use crate::{util::{discover_process_passive_sockets, get_ports_for_inodes}, default_handle_read_err, os_helper};

struct SleepingTask {
    token: u64,
    pgid: i32,
    cmdline: Vec<CString>,

    tcp_listeners: Vec<(TcpListener, Vec<TcpStream>)>,
    already_sent_alarm: bool,
}

#[derive(Debug)]
struct RunningTask {
    token: u64,
    pgid: i32,

    cmdline: Vec<CString>,
}

struct StartingTask {
    token: u64,

    stream: UnixStream,

    start_time: Instant,
    cmdline: Vec<CString>,
}

struct StoppingTask {

    token: u64,
    ports: Vec<u32>,
    established_listeners: Vec<TcpListener>,
    pgid: i32,
    cmdline: Vec<CString>,
    last_activity: Instant,

}

struct NewListenerConnection {

    stream: UnixStream,
    connection_time: Instant,

}

pub struct Scheduler {
    running_tasks: Vec<RunningTask>,
    sleeping_tasks: Vec<SleepingTask>,
    starting_tasks: Vec<StartingTask>,
    stopping_tasks: Vec<StoppingTask>,

    new_listener_connections: Vec<NewListenerConnection>,
    listener: UnixListener,

    cache: Cache
}

impl Scheduler {

    pub fn new()-> Result<Self, ()> {
        fs::create_dir_all("/tmp/systemc/").unwrap();
        let listener = UnixListener::bind("/tmp/systemc/scheduler.sock");
        if let Ok(listener) = listener {
                listener.set_nonblocking(true).unwrap();
                return Ok(Scheduler {
                    running_tasks: Vec::new(),
                    sleeping_tasks: Vec::new(),
                    starting_tasks: Vec::new(),
                    stopping_tasks: Vec::new(),
                    new_listener_connections: Vec::new(),
                    listener,
                    cache: Cache::new(),
                });
        }
        Err(())
    }

    // TODO make the values here configurable
    // and achieve (optional) multithreading for check_active_services()
    // to improve the maximum speed update() can have
    // so that running application with extremely hard real-time
    // constraints is also possible

    /// should take 50ms
    pub fn update(&mut self) {
        for _ in 0..4 {
            self.check_sleeping_services();
            self.check_starting_services();
            sleep(Duration::from_millis(10));
        }
        self.check_active_services();
        self.check_stopping_tasks();
        self.check_new_connections();
        self.cache.gc_cyles += 1;
        if self.cache.gc_cyles > 200 {
            self.cache.gc_cyles = 0;
            self.gc();
        }
    }

    fn gc(&mut self) {
        for i in (0..self.starting_tasks.len()).rev() {
            //TODO make configurable
            if self.starting_tasks.get(i).unwrap().start_time.elapsed() > Duration::from_secs(300) {
                self.starting_tasks.swap_remove(i);
            }
        }
    }

    fn check_active_services(&mut self) {
        let mut active_socket_inodes = HashMap::new();
        let tcp_socks = fs::read_to_string("/proc/net/tcp").unwrap();
        let mut lines = tcp_socks.lines();
        lines.next();
        let mut not_reached_active_sockets = true;
        for line in lines {
            if not_reached_active_sockets && line[20..33] == *"00000000:0000" {
                continue;
            }
            not_reached_active_sockets = false;
            let len = line.len();
            let inode = &line[92..len];
            let inode = inode.split_once(" ").unwrap().0;
            active_socket_inodes.insert(inode, ());
        }
        let l = self.cache.process_groups.len();
        for i_outer in (0..l).rev() {
            let (pgid, pids, time) = self.cache.process_groups.get_mut(i_outer).unwrap();
            let pgid = *pgid;
            let mut active = false;
            'p_loop: for (_, inodes) in &*pids {
                for inode in inodes {
                    let inode = inode.to_string();
                    if active_socket_inodes.contains_key(&inode.as_str()) {
                        active = true;
                        break 'p_loop;
                    }
                }
            }
            if active {
                *time = Instant::now();
            } else {
                if time.elapsed() > Duration::from_secs(5) {
                    'inner: for i in 0..self.running_tasks.len() {
                        if self.running_tasks.get(i).unwrap().pgid == pgid {
                            let mut process_groups = Vec::with_capacity(self.running_tasks.len());
                            for running in &self.running_tasks {
                                process_groups.push(running.pgid);
                            }
                            let task = self.running_tasks.swap_remove(i);
                            self.cache.repopulate_cycles = 0;
                            discover_process_passive_sockets(&mut self.cache, &process_groups);
                            let mut i_outer = self.cache.process_groups.len();
                            if i_outer > 0 {
                                i_outer -= 1;
                                loop {
                                    if self.cache.process_groups.get(i).unwrap().0 == pgid {
                                        break;
                                    }
                                    if i_outer > 0 {
                                        i_outer -= 1;
                                    } else {
                                        break 'inner;
                                    }
                                }
                                let cached_task = self.cache.process_groups.swap_remove(i_outer);
                                self.stopping_tasks.push(StoppingTask {
                                    token: task.token,
                                    established_listeners: Vec::new(),
                                    ports: get_ports_for_inodes((&cached_task.0, &cached_task.1)),
                                    cmdline: task.cmdline,
                                    last_activity: Instant::now(),
                                    pgid: 0,
                                });
                                unsafe {
                                    libc::killpg(task.pgid, SIGCONT);
                                    libc::killpg(task.pgid, SIGTERM);
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        self.cache.repopulate_cycles += 1;
        if self.cache.repopulate_cycles > 600 {
            let mut process_groups = Vec::with_capacity(self.running_tasks.len());
            for running in &self.running_tasks {
                process_groups.push(running.pgid);
            }
            discover_process_passive_sockets(&mut self.cache, &process_groups);
            self.cache.repopulate_cycles = 0;
            println!("pgs: {:?}", self.cache.process_groups);
        }
    }

    fn check_sleeping_services(&mut self) {
        let mut i = self.sleeping_tasks.len();
        if i > 0 {
            i -= 1;
            loop {
                let task = self.sleeping_tasks.get_mut(i).unwrap();
                for listener in &mut task.tcp_listeners {
                    match listener.0.accept() {
                        Ok(conn) => {
                            listener.1.push(conn.0);
                            if !task.already_sent_alarm {
                                task.already_sent_alarm = true;
                                unsafe {
                                    libc::killpg(task.pgid, SIGCONT);
                                    libc::killpg(task.pgid, SIGUSR1);
                                }
                            }
                        }
                        Err(e) => {
                            if e.kind() == ErrorKind::WouldBlock {
                                continue;
                            }
                            self.sleeping_tasks.remove(i);
                            break;
                        }
                    }
                }
                if i > 0 {
                    i -= 1;
                } else {
                    break;
                }
            }
        }
    }

    fn check_starting_services(&mut self) {
        let mut i = self.starting_tasks.len();
        if i > 0 {
            i -= 1;
            let mut buf1 = [0u8; 8];
            let mut buf2 = [0u8; 4];
            loop {
                let task = self.starting_tasks.get_mut(i).unwrap();
                if task.token == 0 {
                    match task.stream.read_exact(&mut buf1) {
                        Ok(()) => {
                            let token = u64::from_le_bytes(buf1);
                            if token == 0 {
                                if unsafe {libc::getrandom(&mut buf1 as *mut _ as *mut _, 8, 0) } != -1 {
                                    task.token = u64::from_le_bytes(buf1);
                                    task.stream.write(&buf1).unwrap();
                                } else {
                                    eprintln!("System random number generator returned an error!");
                                }
                            } else {
                                let l = self.sleeping_tasks.len();
                                for i in (0..l).rev() {
                                    let sleeping_task = self.sleeping_tasks.get(i).unwrap();
                                    if sleeping_task.token == token {
                                        let sleeping_task = self.sleeping_tasks.swap_remove(i);
                                        task.token = token;
                                        task.cmdline = sleeping_task.cmdline;
                                        if let Err(e) = send_args_to_starting_task(&mut task.stream, &mut task.cmdline)
                                            .and(send_fds_to_starting_task(&mut task.stream, sleeping_task.tcp_listeners))
                                        {
                                            eprintln!("Error while trying to wake task up from sleep: {e}");
                                        }
                                    }
                                }
                            }
                        }
                        Err(e) => {
                            default_handle_read_err!(e, "task", {
                                self.starting_tasks.swap_remove(i);
                                if i == self.starting_tasks.len() {
                                    break;
                                }
                            });
                        }
                    }
                } else {
                    match task.stream.read_exact(&mut buf2) {
                        Ok(()) => {
                            let pgid = pid_t::from_le_bytes(buf2);
                            let mut task = self.starting_tasks.swap_remove(i);
                           task.stream.set_nonblocking(false).unwrap();
                            let mut data = 0u8;
                            /*unsafe {
                                os_helper::receive_fildes(task.stream.as_raw_fd(), &mut procfs_fd as *mut _, 1, &mut data as *mut _ as *mut _, 1);
                            }*/
                            let cmdline = if task.cmdline.len() == 0 {
                                let mut cmdline = Vec::new();
                                let mut size = [0u8; mem::size_of::<usize>()];
                                task.stream.read_exact(&mut size).unwrap();
                                let size = usize::from_le_bytes(size);
                                if size > 0 {
                                    let mut bytes = Vec::with_capacity(size);
                                    bytes.resize(size, 0);
                                    task.stream.read_exact(&mut bytes).unwrap();
                                    let mut bytes_ref = &bytes[0..size];
                                    let mut i = 0;
                                    loop {
                                        if *bytes_ref.get(i).unwrap() == 0 {
                                            if i == 0 {
                                                let l = bytes_ref.len();
                                                bytes_ref = &bytes_ref[1..l];
                                                continue;
                                            }
                                            let (until_i, new_bytes_ref) = bytes_ref.split_at(i);
                                            cmdline.push(CString::new(until_i).unwrap());
                                            bytes_ref = new_bytes_ref;
                                            i = 0;
                                            let l = bytes_ref.len();
                                            bytes_ref = &bytes_ref[1..l];
                                            if l == 1 {
                                                break;
                                            }
                                            continue;
                                        }
                                        if i < bytes_ref.len() {
                                            i += 1;
                                        } else {
                                            break;
                                        }
                                    }
                                }
                                cmdline
                            } else {
                                task.cmdline
                            };
                            self.running_tasks.push(RunningTask { token: task.token, pgid, cmdline});
                        }
                        Err(e) => {
                            default_handle_read_err!(e, "task", {
                                self.starting_tasks.swap_remove(i);
                                if i == self.starting_tasks.len() {
                                    break;
                                }
                            });
                        }
                    }
                }
                if i > 0 {
                    i -= 1;
                } else {
                    break;
                }
            }
        }
    }

    fn check_stopping_tasks(&mut self) {
        let now = Instant::now();
        let l = self.stopping_tasks.len();
        'outer: for i_outer in (0..l).rev() {
            let task = self.stopping_tasks.get_mut(i_outer).unwrap();
            //TODO make configurable
            if task.last_activity.elapsed() > Duration::from_secs(300) {
                self.stopping_tasks.remove(i_outer);
                continue;
            }
            let l = task.ports.len();
            for i in (0..l).rev() {
                let port = *task.ports.get(i).unwrap();
                match TcpListener::bind(format!("127.0.0.1:{}", port)) {
                    Ok(listener) => {
                        if listener.set_nonblocking(true).is_err() {
                            panic!("Failed to move socket into nonblocking mode! [9]");
                        }
                        task.last_activity = now;
                        if task.ports.len() == 1 && task.pgid != 0 {
                            let mut task = self.stopping_tasks.swap_remove(i_outer);
                            task.ports.swap_remove(i);
                            let mut i = task.established_listeners.len();
                            let mut tcp_listeners = Vec::with_capacity(i + 1);
                            tcp_listeners.push((listener, Vec::new()));
                            if i > 0 {
                                i -= 1;
                                loop {
                                    tcp_listeners.push((task.established_listeners.remove(i), Vec::new()));
                                    if i > 0 {
                                        i -= 1;
                                    } else {
                                        break;
                                    }
                                }
                            }
                            self.sleeping_tasks.push(SleepingTask {
                                token: task.token,
                                pgid: task.pgid,
                                cmdline: task.cmdline,
                                tcp_listeners,
                                already_sent_alarm: false
                            });
                            continue 'outer;
                        }
                        task.established_listeners.push(listener);
                        task.ports.swap_remove(i);
                    }
                    _ => continue
                }
            }
        }
    }

    fn check_new_connections(&mut self) {
        let mut i = self.new_listener_connections.len();
        let mut buf = [0u8];
        if i > 0 {
            i -= 1;
            loop {
                let new_connection = self.new_listener_connections.get_mut(i).unwrap();
                if new_connection.connection_time.elapsed() > Duration::from_millis(500) {
                    self.new_listener_connections.swap_remove(i);
                    if i > 0 {
                        continue;
                    } else {
                        break;
                    }
                }
                match new_connection.stream.read_exact(&mut buf) {
                    Ok(()) => {
                        match buf[0] {
                            1 => {
                                let task = self.new_listener_connections.swap_remove(i);
                                self.starting_tasks.push(StartingTask { token: 0, stream: task.stream, start_time: Instant::now(), cmdline: Vec::new() })
                            }
                            2 => {
                                drop(new_connection.stream.set_read_timeout(Some(Duration::from_millis(5))));
                                drop(new_connection.stream.set_nonblocking(false));
                                let mut token = [1u8; 8];
                                if new_connection.stream.read_exact(&mut token).is_ok() {
                                    let mut connection = self.new_listener_connections.swap_remove(i);
                                    let token = u64::from_le_bytes(token);
                                    let l = self.stopping_tasks.len();
                                    for i in 0..l {
                                        if self.stopping_tasks.get(i).unwrap().token == token {
                                            let task = self.stopping_tasks.get_mut(i).unwrap();
                                            drop(connection.stream.write(&[1]));
                                            let mut pgid = [1u8; 4];
                                            let rslt = connection.stream.read(&mut pgid);
                                            if rslt.is_ok() {
                                                if task.ports.len() == 0 {
                                                    let mut task = self.stopping_tasks.swap_remove(i);
                                                    let mut i = task.established_listeners.len();
                                                    let mut tcp_listeners = Vec::with_capacity(i);
                                                    if i > 0 {
                                                        i -= 1;
                                                        loop {
                                                            tcp_listeners.push((task.established_listeners.remove(i), Vec::new()));
                                                            if i > 0 {
                                                                i -= 1;
                                                            } else {
                                                                break;
                                                            }
                                                        }
                                                    }
                                                    self.sleeping_tasks.push(SleepingTask {
                                                        token,
                                                        pgid: pid_t::from_le_bytes(pgid),
                                                        cmdline: task.cmdline,
                                                        tcp_listeners,
                                                        already_sent_alarm: false
                                                    });
                                                } else {
                                                    task.pgid = pid_t::from_le_bytes(pgid);
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    if i > 0 {
                                        continue;
                                    } else {
                                        break;
                                    }
                                }
                                let mut new_conn = self.new_listener_connections.swap_remove(i);
                                new_conn.stream.write(&[255]).unwrap();
                                if i > 0 {
                                    continue;
                                } else {
                                    break;
                                }
                            }
                            _ => {

                            }
                        }
                    }
                    Err(e) => {
                        if e.kind() != ErrorKind::WouldBlock {
                            eprintln!("Caught error while checking in new IPC connection: {e}");
                        }
                    }
                }
                if i > 0 {
                    i -= 1;
                } else {
                    break;
                }
            }
        }
        loop {
            match self.listener.accept() {
                Ok(stream) => {
                    stream.0.set_nonblocking(true).unwrap();
                    self.new_listener_connections.push(NewListenerConnection { stream: stream.0, connection_time: Instant::now() });
                }
                Err(e) => {
                    if e.kind() == ErrorKind::WouldBlock {
                        break;
                    }
                    eprintln!("Caught error while listening for IPC connections: {e}");
                    break;
                }
            }
        }
    }

}

pub struct Cache {

    pub process_groups: Vec<(pid_t, Vec<(pid_t, Vec<u64>)>, Instant)>,
    pub repopulate_cycles: u16,
    pub fast_check_cycles: u8,
    pub gc_cyles: u8,

}

impl Cache {

    pub const fn new() -> Self {
        Cache {
            process_groups: Vec::new(),
            repopulate_cycles: 0,
            fast_check_cycles: 0,
            gc_cyles: 0,
        }
    }

}

pub fn send_args_to_starting_task(conn: &mut UnixStream, args: &mut Vec<CString>) -> Result<(), std::io::Error> {
    let mut bytes = Vec::new();
    for arg in args {
        bytes.extend_from_slice(arg.as_bytes_with_nul());
    }
    conn.write(&bytes.len().to_le_bytes())?;
    conn.write(&bytes)?;
    Ok(())
}

pub fn send_fds_to_starting_task(conn: &mut UnixStream, mut tcp_listeners: Vec<(TcpListener, Vec<TcpStream>)>) -> Result<(), std::io::Error>{
    // TODO check for limits on length (cuz of ssize_t limit in unix ancillary data
    let mut i = tcp_listeners.len();
    if i > 0 {
        i -= 1;
        conn.write(&(tcp_listeners.get(i).unwrap().1.len() + 1).to_le_bytes()).unwrap();
        let mut next = tcp_listeners.remove(i);
        loop {
            let mut fds = Vec::with_capacity(next.1.len() + 1);
            fds.push(next.0.into_raw_fd());
            for stream in next.1 {
                fds.push(stream.into_raw_fd());
            }
            if i > 0 {
                i -= 1;
                next = tcp_listeners.remove(i);
            } else {
                if unsafe { os_helper::send_fildes(conn.as_raw_fd(), fds.as_ptr(), fds.len(), &0i8 as *const _, 1) } == -1 {
                    return Err(std::io::Error::last_os_error());
                }
                break;
            }
            if unsafe { os_helper::send_fildes(
                conn.as_raw_fd(), fds.as_ptr(), fds.len(),
                &(next.1.len() + 1).to_le_bytes() as *const _ as *const _, std::mem::size_of::<usize>()) } == -1 {
                return Err(std::io::Error::last_os_error());
            }
        }
    }
    Ok(())
}
