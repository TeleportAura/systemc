use std::ffi::{c_int, c_char};

pub use libc::size_t as c_size_t;

extern {
    pub fn send_fildes(socket: c_int, fildes: *const c_int, fildes_len: c_size_t, data: *const c_char, data_len: c_size_t) -> c_int;
    pub fn receive_fildes(socket: c_int, fildes: *const c_int, fildes_len: c_size_t, data: *mut c_char, data_len: c_size_t) -> c_int;
}
