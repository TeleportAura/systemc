fn main() {
    cc::Build::new()
        .file("../systemc-commons/src/oshelper.c")
        .warnings(true)
        .extra_warnings(true)
        .compile("oshelper");
}
