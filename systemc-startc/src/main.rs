use std::ffi::{c_char, CString};
use std::mem::ManuallyDrop;
use std::net::{TcpListener, TcpStream, IpAddr};
use std::os::unix::{prelude::{OsStrExt, AsRawFd, FromRawFd, IntoRawFd}, net::UnixStream};
use std::{env, io::Read, ptr};
use libc::{ptrace, PTRACE_SYSCALL, pid_t, c_void, c_long, c_int, in_port_t, sa_family_t, iovec, user_regs_struct, SIGTRAP, PTRACE_EVENT_FORK, PTRACE_EVENT_VFORK, PTRACE_EVENT_CLONE, PTRACE_GETEVENTMSG, PTRACE_SETOPTIONS, PTRACE_O_TRACESYSGOOD, PTRACE_O_TRACECLONE, PTRACE_O_TRACEFORK, PTRACE_O_TRACEVFORK, PTRACE_GETREGS, PTRACE_TRACEME, size_t, PTRACE_SETREGS, AF_UNIX, SOCK_STREAM, fcntl, F_GETFL, O_NONBLOCK};
use core::mem::{MaybeUninit, self};
use std::io::{Write, self, ErrorKind};

fn perror(eid: u32) {
        println!("error, exiting: {}", eid);
        unsafe {libc::perror(ptr::null())};
}

macro_rules! call_c_function {
        ($c_func: expr) => {
            {
                if $c_func == -1 {
                    perror(17);
                    return Err(std::io::Error::last_os_error());
                }
            }
        };
}

fn main() -> Result<(), io::Error> {

    let mut daemon_conn = match UnixStream::connect("/tmp/systemc/scheduler.sock") {
        Ok(conn) => conn,
        Err(e) => {
            if e.kind() == ErrorKind::NotFound {
                eprintln!("The systemc daemon has to be running!");
            }
            return Err(e);
        }
    };

    let mut ipc_connection = MaybeUninit::uninit();
    unsafe {
        libc::socketpair(AF_UNIX, SOCK_STREAM, 0, ipc_connection.as_mut_ptr() as *mut _);
        // FDs: 4,5
    }
    let ipc_connection: [c_int; 2] = unsafe {
        ipc_connection.assume_init()
    };

    daemon_conn.write(&[1])?;

    let mut listeners = Vec::new();
    let mut arg_bytes = Vec::new();

    let owned_execve_args = match env::var("SYSTEMC_SESSION_TOKEN") {
        Ok(vl) => {
            daemon_conn.write(&vl.parse::<u64>().unwrap().to_le_bytes()).unwrap();
            let mut size_buf = [0u8; mem::size_of::<usize>()];
            daemon_conn.read_exact(&mut size_buf).unwrap();
            let mut size = usize::from_le_bytes(size_buf);
            let mut args = Vec::with_capacity(1);
            if size > 0 {
                let mut bytes = Vec::new();
                bytes.resize_with(size, || 0);
                daemon_conn.read_exact(&mut bytes).unwrap();
                let mut bytes_ref = &bytes[0..size];
                let mut i = 0;
                loop {
                    if *bytes_ref.get(i).unwrap() == 0 {
                        if i == 0 {
                            let l = bytes_ref.len();
                            bytes_ref = &bytes_ref[1..l];
                            continue;
                        }
                        let (until_i, new_bytes_ref) = bytes_ref.split_at(i);
                        args.push(CString::new(until_i).unwrap());
                        bytes_ref = new_bytes_ref;
                        i = 0;
                        let l = bytes_ref.len();
                        bytes_ref = &bytes_ref[1..l];
                        if l == 1 {
                            break;
                        }
                        continue;
                    }
                    if i < bytes_ref.len() {
                        i += 1;
                    } else {
                        break;
                    }
                }
                daemon_conn.read_exact(&mut size_buf)?;
                size = usize::from_le_bytes(size_buf);
                while size != 0 {
                    let mut temp_socks = Vec::with_capacity(size);
                    if unsafe { receive_fildes(daemon_conn.as_raw_fd(), temp_socks.as_ptr(), size, &mut size_buf as *mut _ as *mut _, mem::size_of::<usize>()) } == -1 {
                        panic!("(main.rs//81): {}", io::Error::last_os_error());
                    }
                    unsafe {
                        temp_socks.set_len(size);
                    }
                    size = usize::from_le_bytes(size_buf);
                    let listener_fd = temp_socks.swap_remove(0);
                    let listener = unsafe {
                        TcpListener::from_raw_fd(listener_fd)
                    };
                    let port = listener.local_addr()?.port();
                    listeners.push((Option::Some(listener), port, unsafe {fcntl(listener_fd, F_GETFL)} & O_NONBLOCK == 0, 0 as c_int, temp_socks));
                }
            }

            ManuallyDrop::new(OwnedExecveArgs::from_arg_vec(args))
        }
        _ => {
            if let Some(var) = env::var_os("SYSTEMC_PWD") {
                let mut invoke_command = Vec::from(var.as_bytes());
                invoke_command.extend_from_slice(b"/systemc-cmdline");
                let invoke_command = CString::new(invoke_command).unwrap();

                let mut session_id = [0u8; 8];
                daemon_conn.write(&session_id).unwrap();
                daemon_conn.read_exact(&mut session_id).unwrap();
                env::set_var("SYSTEMC_SESSION_TOKEN", u64::from_le_bytes(session_id).to_string());

                arg_bytes.extend_from_slice(invoke_command.as_bytes_with_nul());

                let execve_args = ManuallyDrop::new(OwnedExecveArgs::from_own_args(invoke_command));
                for arg in &execve_args.args {
                    arg_bytes.extend_from_slice(arg.as_bytes_with_nul());
                }
                execve_args
            } else {
                eprintln!("Environment variable SYSTEMC_PWD needs to be set to the directory where the systemc configuration for this task is located!");
                std::process::exit(-1);
            }
        }
    };

    let mut ipc_connection = unsafe { UnixStream::from_raw_fd(ipc_connection[1]) };
    
    let child = unsafe {
        libc::fork()
    };
    if child == -1 {
        eprintln!("Fork failed!");
        return Err(io::Error::last_os_error());
    }
    
    if child == 0 {
        let mut len = listeners.len();
        let mut opcode_buf = [0u8];
        let mut port_buf = [0u8; 2];
        let mut i32_buf = [0u8; 4];
        'a: while len > 0 {
            ipc_connection.read(&mut opcode_buf)?;
            match opcode_buf[0] {
                1 => {
                    ipc_connection.read_exact(&mut port_buf)?;
                    let port = u16::from_be_bytes(port_buf);
                    for (tcp_listener, port2, blocking, fd, __) in &mut listeners {
                        if port == *port2 {
                            let tcp_listener = tcp_listener.take();
                            if let Some(tcp_listener) = tcp_listener {
                                tcp_listener.set_nonblocking(*blocking)?;
                                let fildes = tcp_listener.into_raw_fd();
                                let data = 42u8;
                                if unsafe {
                                    send_fildes(ipc_connection.as_raw_fd(), &fildes as *const _ as *const _, 1, &data as *const _ as *const _, 1)
                                } == -1 {
                                    ipc_connection.write(&[2])?;
                                    return Err(io::Error::last_os_error());
                                }
                                ipc_connection.read_exact(&mut i32_buf)?;
                                *fd = c_int::from_le_bytes(i32_buf);
                                continue 'a;
                            }
                        }
                    }
                    ipc_connection.write(&[1])?;
                }
                2 => {
                    ipc_connection.read_exact(&mut i32_buf)?;
                    let fd = c_int::from_le_bytes(i32_buf);
                    for (_, _, _, fd2, connections) in &mut listeners {
                        if fd == *fd2 {
                            let l = connections.len();
                            // ig most applications will bind to only one port anyway, so why bother with removing
                            if l > 0 {
                                let fildes = connections.remove(l - 1);
                                let data = 42u8;
                                if unsafe {
                                    send_fildes(ipc_connection.as_raw_fd(), &fildes as *const _ as *const _, 1, &data as *const _ as *const _, 1)
                                } == -1 {
                                    ipc_connection.write(&[2])?;
                                    return Err(io::Error::last_os_error());
                                }
                                if l == 1 {
                                    len -= 1;
                                }
                                continue 'a;
                            }
                        }
                    }
                    ipc_connection.write(&[1])?;
                }
                _ => {
                    eprintln!("Received invalid opcode! ({})", opcode_buf[0]);
                    ipc_connection.write(&[2])?;
                    break;
                }
            }
        }
        
        ipc_connection.write(&[2])?;
        ipc_connection.shutdown(std::net::Shutdown::Both);

        let pgid = unsafe {
            libc::getpgrp()
        };
        daemon_conn.write(&pgid.to_le_bytes())?;

        if arg_bytes.len() > 0 {
            daemon_conn.write(&arg_bytes.len().to_le_bytes())?;
            daemon_conn.write(&arg_bytes)?;
        }

        return Ok(());
    }

    unsafe {
        // workaround for a weird issue that Rust's abstraction over stdout has
        libc::printf(ptr::null());
        libc::execvpe(owned_execve_args.get_location_pointer(), owned_execve_args.get_args_pointer(), owned_execve_args.get_env_vars_pointer());
    }
    println!("failed to execute process!");
    println!("debug info: {:?}", owned_execve_args);
    return Err(io::Error::last_os_error());
}


// all of this is heap allocated, meaning that our pointers actually stay valid
#[allow(dead_code)]
#[derive(Debug)]
struct OwnedExecveArgs {

    location: CString,
    pre_args: Vec<CString>,
    args: Vec<CString>,
    args_pointer: Vec<*const c_char>,
    env_vars: Vec<CString>,
    env_vars_pointer: Vec<*const c_char>,

}

impl OwnedExecveArgs {

    pub fn from_arg_vec(args: Vec<CString>) -> Self {
        let home_dir = &env::var("HOME").expect("Environment variable \"HOME\" is not set!");
        let mut bytes = Vec::new();
        bytes.extend_from_slice(home_dir.as_bytes());
        bytes.extend_from_slice(b"/.local/lib/systemc/systemc-shimc.so");
        let pre_args = vec![CString::new("--preload").unwrap(), CString::new(bytes).unwrap()];
        let mut env_vars = Vec::new();
        let mut needto_set_ld_path = true;
        for var in env::vars_os() {
            let mut bytes = Vec::with_capacity(var.0.len() + var.1.len());
            bytes.extend_from_slice(var.0.as_bytes());
            bytes.extend_from_slice(b"=");
            bytes.extend_from_slice(var.1.as_bytes());
            if let Some(var_key) = var.0.to_str() {
                match var_key {
                    "LD_LIBRARY_PATH" => {
                        if needto_set_ld_path {
                            bytes.extend_from_slice(b":");
                            bytes.extend_from_slice(home_dir.as_bytes());
                            bytes.extend_from_slice(b"/.local/lib/systemc/");
                            needto_set_ld_path = false;
                        }
                    }
                    "PATH" => {
                        bytes.extend_from_slice(b":");
                        bytes.extend_from_slice(home_dir.as_bytes());
                        bytes.extend_from_slice(b"/.local/bin/systemc");
                    }
                    _ => {}
                }
            }
            env_vars.push(CString::new(bytes).unwrap());
        }
        if needto_set_ld_path {
            env_vars.push(CString::new("LD_LIBRARY_PATH=".to_string() +
                home_dir + "/.local/lib/systemc/").unwrap());
        }
        let mut args_pointer = Vec::with_capacity(args.len() + 4);
        let location = CString::new("ld.so").unwrap();
        args_pointer.push(location.as_ptr());
        // we could also just calc the addresses
        args_pointer.push(pre_args.get(0).unwrap().as_ptr());
        args_pointer.push(pre_args.get(1).unwrap().as_ptr());
        for arg in &args {
            args_pointer.push(arg.as_ptr());
        }
        args_pointer.push(ptr::null());
        let mut env_vars_pointer = Vec::with_capacity(env_vars.len() + 1);
        for env_var in &env_vars {
        env_vars_pointer.push(env_var.as_ptr());
        }
        env_vars_pointer.push(ptr::null());
        Self {
            location,
            pre_args,
            args,
            args_pointer,
            env_vars,
            env_vars_pointer,
        }
    }

    pub fn from_own_args(location: CString) -> Self {
        let mut args_os = env::args_os();
        args_os.next();
        let mut args = vec![location];
        for arg in args_os {
            args.push(CString::new(arg.as_bytes()).unwrap());
        }
        Self::from_arg_vec(args)
    }

    /// SAFETY: pointer is only guaranteed to be valid for as long as the self pointer supplied to this function is
    unsafe fn get_location_pointer(&self) -> *const c_char {
        self.location.as_ptr()
    }

    /// SAFETY: pointer is only guaranteed to be valid for as long as the self pointer supplied to this function is
    unsafe fn get_args_pointer(&self) -> *const *const c_char {
        self.args_pointer.as_ptr()
    }

    /// SAFETY: pointer is only guaranteed to be valid for as long as the self pointer supplied to this function is
    unsafe fn get_env_vars_pointer(&self) -> *const *const c_char {
        self.env_vars_pointer.as_ptr()
    }

}

extern {

    pub fn send_fildes(socket: c_int, fildes: *const c_int, fildes_len: size_t, data: *const c_char, data_len: size_t) -> c_int;
    pub fn receive_fildes(socket: c_int, fildes: *const c_int, fildes_len: size_t, data: *mut c_char, data_len: size_t) -> c_int;

}

#[allow(unreachable_code)]
pub fn send_tcp_listener(sockfd: c_int, listener: TcpListener, open_connections: Vec<(TcpStream, Vec<u8>)>) -> Result<(), io::Error>{
    let mut data_sizes = Vec::with_capacity(open_connections.len());
    for (_, data) in &open_connections {
        for byte in loop {
                #[cfg(target_endian = "little")]
                break data.len().to_le_bytes();
                break data.len().to_be_bytes();
            } {
            data_sizes.push(byte);
        }
    }

    call_c_function!(unsafe { libc::write(sockfd, &open_connections.len().to_le_bytes() as *const _ as *const _, mem::size_of::<usize>()) });
    call_c_function!(unsafe { send_fildes(sockfd, &listener.into_raw_fd() as *const _ as *const _, 1,
                                    data_sizes.as_ptr() as *const _ as *const _, data_sizes.len())});
    for (tcp_stream, mut data) in open_connections {
        data.reserve_exact(6);
        let local_addr = tcp_stream.local_addr().unwrap();
        data.extend_from_slice(&local_addr.port().to_be_bytes());
        match local_addr.ip() {
            IpAddr::V4(addr) => {
                data.extend_from_slice(&addr.octets());
            },
            _ => {
                panic!("(util.rs//send_tcp_listener//1): this shouldn't happen");
            }
        }
        call_c_function!(unsafe { send_fildes(sockfd, &tcp_stream.into_raw_fd() as *const _ as *const _ , 1, data.as_ptr() as *const _, data.len()) });
    }

    Ok(())
}
