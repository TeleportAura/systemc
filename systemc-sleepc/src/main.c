#define _GNU_SOURCE

#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <signal.h>

void catch_alarm(int sig) {
    char *newargv[] = {"systemc-startc", NULL};
    execvp("systemc-startc", newargv);
    perror("Failed to execute systemc-startc!");
}

int main(int argc, int *argv[]) {
    signal(SIGUSR1, catch_alarm);
    pause();
}
